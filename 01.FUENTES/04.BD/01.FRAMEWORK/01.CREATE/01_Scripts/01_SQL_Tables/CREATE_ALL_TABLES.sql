SPOOL salida_CREATE_all_tables.txt

--------------------------------------------------------
--  Common FRW DATA
--------------------------------------------------------

-- Common Tables
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CHANNEL.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_COUNTRY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ENTERPRISE.sql

-- Canonical Error
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CANONICAL_ERROR_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CANONICAL_ERROR.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_STATUS_TYPE.sql

-- SERVICE Tables
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SERVICE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CAPABILITY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SERVICES_DETAILS_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CAPABILITY_DETAILS_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SERVICES_DETAILS.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CAPABILITY_DETAILS.sql

-- SYSTEM Tables
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SYSTEM.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SYSTEM_API.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SYSTEM_API_OPERATION.sql

-- CONSUMER Tables
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONSUMER.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONSUMER_DETAILS_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONSUMER_CAP_DETAILS_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONSUMER_DETAILS.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONSUMER_CAP_DETAILS.sql



--------------------------------------------------------
--  ParameterManager
--------------------------------------------------------
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CDM_ENTITY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CDM_FIELD.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONFIG.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONFIG_PROPERTY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONFIG_LIST.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ENDPOINT_TRANSPORT.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ENDPOINT.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_PARAMETER.sql


--------------------------------------------------------
--  MessageManager
--------------------------------------------------------
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CHECK_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CAPABILITY_CHECK.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CHECK_CONFIG.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_DEFAULT_CHECK.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_MESSAGE_TRANSACTION.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_TRANSACTION_CHECK.sql

--------------------------------------------------------
--  LoggerManager
--------------------------------------------------------
--@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_AUDIT_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_SEVERITY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_LOG_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONVERSATION.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_TRACE.sql
--@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_AUDIT.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_LOG.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_MAPPING.sql

--------------------------------------------------------
--  ErrorManager
--------------------------------------------------------

-- Canonical Error
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_MAPPING.sql

-- ErrorHospital

@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_TREATMENT_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_DERIVATION_CONF.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_CONVERSATION.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_THERAPY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_TREATMENT_PLAN.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_DIAGNOSIS.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_HOSPITAL.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERROR_DISPATCHER.sql

--  RetryManager

@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_RETRYMANAGER_CONFIG.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_RETRYMANAGER.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_RETRYMANAGER_HISTORY.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_RETRY_NOTIFICATION.sql

--  Diagnose

@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERR_ACTION.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERR_ACTION_CFG.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERR_CMP.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERR_CMP_CFG.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_ERR_DIAGNOSTICS.sql

--------------------------------------------------------
--  CorrelationManager
--------------------------------------------------------

@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CORRELATION_MEMBER_TYPE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CORRELATION_INSTANCE.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CORRELATION_GROUP.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CORRELATION.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CORRELATION_GROUP_FLAGS.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CORRELATION_MEMBER_FLAGS.sql

--------------------------------------------------------
--  ConversationManager
--------------------------------------------------------
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONVERSATION_STATUS.sql
@.\01_Scripts\01_SQL_Tables\CREATE_TABLE_ESB_CONVERSATION_CB_GROUP.sql

SPOOL OFF;
/