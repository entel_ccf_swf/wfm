#!/bin/bash
DIR=`dirname $0`
DIR=`realpath $DIR`

function prop {
    grep "${1}" $DIR/INT_OSB_VitalityChecks-1.0.properties|cut -d'=' -f2
}

(crontab -l 2>/dev/null; echo "$(prop 'JMSPathServiceCrontabMask') $DIR/INT_OSB_VitalityChecks-1.0_JMSPathServiceVitalityCheck.sh") | crontab -