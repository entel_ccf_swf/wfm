xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)
declare namespace ns1="http://www.entel.cl/SC/RoutingManager/route/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/route_RoutingManager_v1_CSM.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace rou = "http://www.entel.cl/SC/CorrelationManager/Aux/RoutingMetadata";

declare namespace ns5 = "http://www.entel.cl/EDD/Dictionary/v1";

declare variable $RouteREQ as element() (:: schema-element(ns1:RouteREQ) ::) external;
declare variable $Result as element() (:: schema-element(ns2:Result) ::) external;
declare variable $RSPMsg as element() external;

declare function local:get_RoutingManager_RSP($RouteREQ as element() (:: schema-element(ns1:RouteREQ) ::), 
                                              $Result as element() (:: schema-element(ns2:Result) ::), 
                                              $RSPMsg as element()) 
                                              as element() (:: schema-element(ns1:RouteRSP) ::) {
    <ns1:RouteRSP>
        {$Result}
        {$RouteREQ/ns3:RequestHeader}
        {$RouteREQ/rou:RouteMeta}
        {$RouteREQ/rou:RouteTo}
        {$RouteREQ/ns1:REQMsg}
        <ns1:RSPMsg>{$RSPMsg}</ns1:RSPMsg>
    </ns1:RouteRSP>
};

local:get_RoutingManager_RSP($RouteREQ, $Result, $RSPMsg)
