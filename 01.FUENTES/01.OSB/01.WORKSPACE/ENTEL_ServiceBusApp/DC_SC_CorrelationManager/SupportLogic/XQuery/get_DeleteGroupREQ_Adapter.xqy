xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/deleteGroup/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/deleteGroup_CorrelationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/deleteGroup";
(:: import schema at "../JCA/deleteGroup/deleteGroup_sp.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $SC_REQ as element() (:: schema-element(ns1:DeleteGroupREQ) ::) external;

declare function local:func($SC_REQ as element() (:: schema-element(ns1:DeleteGroupREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_GRP_TAG>{fn:data($SC_REQ/cor:GroupTag)}</ns2:P_GRP_TAG>
    </ns2:InputParameters>
};

local:func($SC_REQ)
