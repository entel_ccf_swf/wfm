xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/createGroupFlags/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/createGroupFlags_CorrelationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/createGroupFlags";
(:: import schema at "../JCA/createGroupFlags/createGroupFlags_sp.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $SC_REQ as element() (:: schema-element(ns1:CreateGroupFlagsREQ) ::) external;

declare function local:func($SC_REQ as element() (:: schema-element(ns1:CreateGroupFlagsREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {

      <ns2:InputParameters>
         
          <ns2:P_GRP_FLAGS_CUR>
              {
                  for $Flag in $SC_REQ/cor:Flags/cor:Flag
                  return 
                  <ns2:P_GRP_FLAGS_CUR_ITEM>
                      <ns2:NAME_>{fn:data($Flag/@name)}</ns2:NAME_>
                      <ns2:VALUE_>{fn:data($Flag/@value)}</ns2:VALUE_></ns2:P_GRP_FLAGS_CUR_ITEM>
              }
          </ns2:P_GRP_FLAGS_CUR>
          <ns2:P_GRP_TAG>{fn:data($SC_REQ/cor:GroupTag)}</ns2:P_GRP_TAG>
      </ns2:InputParameters>
};

local:func($SC_REQ)
