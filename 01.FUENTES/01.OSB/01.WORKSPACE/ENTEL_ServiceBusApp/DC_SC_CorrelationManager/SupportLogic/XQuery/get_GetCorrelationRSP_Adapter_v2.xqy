xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/CorrelationManager/getCorrelation/v2";
(:: import schema at "../../SupportAPI/XSD/CSM/getCorrelation_CorrelationManager_v2_CSM.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getCorrelationMemberDBAdapter_v2";
(:: import schema at "../JCA/getCorrelationMemberAdapter_v2/getCorrelationMemberDBAdapter_v2_sp.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";

declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $OutputParameters as element() (:: schema-element(ns1:OutputParameters) ::) external;

declare variable $Result as element() (:: schema-element(ns3:Result) ::)  external;

declare function local:func($OutputParameters as element()? (:: schema-element(ns1:OutputParameters) ::) , $Result as element() (:: schema-element(ns3:Result) ::)  ) as element() (:: schema-element(ns2:GetCorrelationRSP) ::) {
 
    let $Group := $OutputParameters/ns1:P_GROUP
    
    return
 
    <ns2:GetCorrelationRSP>
            {$Result}
            {
            if(fn:data($Group/ns1:GRP_NAME_)!='') then
              <cor:Group cor:groupName="{fn:data($Group/ns1:GRP_NAME_)}">
                  {
                      if ($Group/ns1:GRP_TAG_)
                      then attribute cor:groupTag {fn:data($Group/ns1:GRP_TAG_)}
                      else ()
                  }
                  {
                      if ($Group/ns1:GRP_STATUS_)
                      then attribute cor:groupStatus {fn:data($Group/ns1:GRP_STATUS_)}
                      else ()
                  }
                  {
                      if (exists($Group/ns1:GRP_DESC_) and data($Group/ns1:GRP_DESC_) != '')
                      then attribute cor:groupDescription {fn:data($Group/ns1:GRP_DESC_)}
                      else ()
                  }
                  <cor:Members>
                      {
                          for $GRP_MEMS__ITEM in $Group/ns1:GRP_MEMS_/ns1:GRP_MEMS__ITEM
                          return 
                          <cor:Member 
                              cor:memberCorrID="{fn:data($GRP_MEMS__ITEM/ns1:MEM_CORR_ID_)}"
                              cor:memberType="{fn:data($GRP_MEMS__ITEM/ns1:MEM_TYPE_)}"
                              cor:memberName="{fn:data($GRP_MEMS__ITEM/ns1:MEM_NAME_)}">
                              {
                                  if (exists($GRP_MEMS__ITEM/ns1:MEM_ADDR_) and data($GRP_MEMS__ITEM/ns1:MEM_ADDR_) != '')
                                  then attribute cor:memberAddress {fn:data($GRP_MEMS__ITEM/ns1:MEM_ADDR_)}
                                  else ()
                              }
                              {
                                  if (exists($GRP_MEMS__ITEM/ns1:MEM_LABEL_) and data($GRP_MEMS__ITEM/ns1:MEM_ADDR_) != '')
                                  then attribute cor:memberLabel {fn:data($GRP_MEMS__ITEM/ns1:MEM_LABEL_)}
                                  else ()
                              }
                              {
                                if (fn:exists($GRP_MEMS__ITEM/ns1:MEM_FLAGS_/ns1:MEM_FLAGS__ITEM/ns1:NAME_)) 
                                  then 
                                      <cor:Flags>
                                          {
                                              for $MEM_FLAGS__ITEM in $GRP_MEMS__ITEM/ns1:MEM_FLAGS_/ns1:MEM_FLAGS__ITEM
                                              return 
                                              <cor:Flag name="{fn:data($MEM_FLAGS__ITEM/ns1:NAME_)}" value="{fn:data($MEM_FLAGS__ITEM/ns1:VALUE_)}"></cor:Flag>
                                          }
                                      </cor:Flags>
                                  else
                                    ()
                              }
                              
                              </cor:Member>
                      }
                  </cor:Members>
                  {
                    if(fn:exists($Group/ns1:GRP_FLAGS_/ns1:MEM_FLAGS__ITEM/ns1:NAME_))
                      then
                        <cor:Flags>
                            {
                                for $MEM_FLAGS__ITEM1 in $Group/ns1:GRP_FLAGS_/ns1:MEM_FLAGS__ITEM
                                return 
                                <cor:Flag name="{fn:data($MEM_FLAGS__ITEM1/ns1:NAME_)}" value="{fn:data($MEM_FLAGS__ITEM1/ns1:VALUE_)}"></cor:Flag>
                            }
                        </cor:Flags>
                      else
                        ()
                  }
                  {
                      if ($Group/ns1:GRP_HEADER_)
                      then <cor:GroupHeader>{fn:data($Group/ns1:GRP_HEADER_)}</cor:GroupHeader>
                      else ()
                  }
              </cor:Group>
            else
              ()
            } 
    </ns2:GetCorrelationRSP>
};

local:func($OutputParameters,$Result)