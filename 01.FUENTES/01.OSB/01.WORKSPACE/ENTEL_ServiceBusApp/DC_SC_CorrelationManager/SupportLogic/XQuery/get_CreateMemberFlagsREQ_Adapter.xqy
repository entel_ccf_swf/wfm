xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/createMemberFlags/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/createMemberFlags_CorrelationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/createMemberFlags";
(:: import schema at "../JCA/createMemberFlags/createMemberFlags_sp.xsd" ::)

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $SC_REQ as element() (:: schema-element(ns1:CreateMemberFlagsREQ) ::) external;

declare function local:func($SC_REQ as element() (:: schema-element(ns1:CreateMemberFlagsREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {

      <ns2:InputParameters>
          <ns2:P_MEM_CORR_ID>{fn:data($SC_REQ/cor:MemberCorrID)}</ns2:P_MEM_CORR_ID>
          <ns2:P_MEM_NAME>{fn:data($SC_REQ/cor:MemberName)}</ns2:P_MEM_NAME>
          <ns2:P_MEM_TYPE>{fn:data($SC_REQ/cor:MemberType)}</ns2:P_MEM_TYPE>
          <ns2:P_GRP_TAG>{fn:data($SC_REQ/cor:GroupTag)}</ns2:P_GRP_TAG>
          <ns2:P_MEM_FLAGS_CUR>
              {
                  for $Flag in $SC_REQ/cor:Flags/cor:Flag
                  return 
                  <ns2:P_MEM_FLAGS_CUR_ITEM>
                      <ns2:NAME_>{fn:data($Flag/@name)}</ns2:NAME_>
                      <ns2:VALUE_>{fn:data($Flag/@value)}</ns2:VALUE_></ns2:P_MEM_FLAGS_CUR_ITEM>
              }
          </ns2:P_MEM_FLAGS_CUR>
         
      </ns2:InputParameters>
};

local:func($SC_REQ)
