xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/CorrelationManager/registerGroup/v2";
(:: import schema at "../../SupportAPI/XSD/CSM/registerGroup_CorrelationManager_v2_CSM.xsd" ::)

declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/registerCorrelationMemberAdapter_v2";
(:: import schema at "../JCA/registerCorrelationMemberAdapter_v2/registerCorrelationMemberAdapter_v2_sp.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace cor = "http://www.entel.cl/SC/CorrelationManager/Aux/CorrelationMembers";

declare variable $SC_REQ as element() (:: schema-element(ns1:RegisterGroupREQ) ::) external;

declare function local:func($SC_REQ as element() (:: schema-element(ns1:RegisterGroupREQ) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {

  let $Group := $SC_REQ/cor:Group
  
  return
    
  <ns2:InputParameters>
    <ns2:P_GROUP>
        <ns2:GRP_NAME_>{fn:data($Group/@cor:groupName)}</ns2:GRP_NAME_>
        {
            if ($Group/@cor:groupTag)
            then <ns2:GRP_TAG_>{fn:data($Group/@cor:groupTag)}</ns2:GRP_TAG_>
            else ()
        }
        {
            if ($Group/cor:GroupHeader)
            then <ns2:GRP_HEADER_>{fn:data($Group/cor:GroupHeader)}</ns2:GRP_HEADER_>
            else ()
        }
        {
            if ($Group/@cor:groupStatus)
            then <ns2:GRP_STATUS_>{fn:data($Group/@cor:groupStatus)}</ns2:GRP_STATUS_>
            else ()
        }
        {
            if ($Group/@cor:groupDescription)
            then <ns2:GRP_DESC_>{fn:data($Group/@cor:groupDescription)}</ns2:GRP_DESC_>
            else ()
        }
        <ns2:GRP_MEMS_>
            {
                for $Member in $Group/cor:Members/cor:Member
                return 
                <ns2:GRP_MEMS__ITEM>
                    <ns2:MEM_NAME_>{fn:data($Member/@cor:memberName)}</ns2:MEM_NAME_>
                    {
                        if ($Member/@cor:memberLabel)
                        then <ns2:MEM_LABEL_>{fn:data($Member/@cor:memberLabel)}</ns2:MEM_LABEL_>
                        else ()
                    }
                    <ns2:MEM_TYPE_>{fn:data($Member/@cor:memberType)}</ns2:MEM_TYPE_>
                    <ns2:MEM_CORR_ID_>{fn:data($Member/@cor:memberCorrID)}</ns2:MEM_CORR_ID_>
                    <ns2:MEM_ADDR_>{fn:data($Member/@cor:memberAddress)}</ns2:MEM_ADDR_>
                    {
                    if (fn:exists($Member/cor:Flags/cor:Flag)) then
                      <ns2:MEM_FLAGS_>
                          {
                              for $Flag in $Member/cor:Flags/cor:Flag
                              return 
                              <ns2:MEM_FLAGS__ITEM>
                                  <ns2:NAME_>{fn:data($Flag/@name)}</ns2:NAME_>
                                  <ns2:VALUE_>{fn:data($Flag/@value)}</ns2:VALUE_></ns2:MEM_FLAGS__ITEM>
                          }
                      </ns2:MEM_FLAGS_>
                    else()
                    }
                  </ns2:GRP_MEMS__ITEM>
            }</ns2:GRP_MEMS_>
        {
        if (fn:exists($Group/cor:Flags/cor:Flag)) then
          <ns2:GRP_FLAGS_>
              {
                  for $Flag1 in $Group/cor:Flags/cor:Flag
                  return 
                  <ns2:MEM_FLAGS__ITEM>
                      <ns2:NAME_>{fn:data($Flag1/@name)}</ns2:NAME_>
                      <ns2:VALUE_>{fn:data($Flag1/@value)}</ns2:VALUE_></ns2:MEM_FLAGS__ITEM>
              }
          </ns2:GRP_FLAGS_>
        else()
        }
    </ns2:P_GROUP>
  </ns2:InputParameters>
    
};

local:func($SC_REQ)
