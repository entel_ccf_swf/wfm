xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)


declare function local:get_Fault_CanonicalError() as element(){
    	<ns3:Result status="ERROR" description="ERROR VALIDO" xmlns:ns3="http://www.entel.cl/ESO/Result/v1">
		<ns4:CanonicalError code="10006" description="ERROR DURANTE EL PROCESAMIENTO DEL ERROR." type="FWNE" xmlns:ns4="http://www.entel.cl/ESO/Error/v1"/>
	</ns3:Result>
};

local:get_Fault_CanonicalError()
