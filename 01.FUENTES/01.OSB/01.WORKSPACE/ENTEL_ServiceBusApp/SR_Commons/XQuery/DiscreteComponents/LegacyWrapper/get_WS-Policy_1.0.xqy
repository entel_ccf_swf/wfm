xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)
declare namespace wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
declare namespace wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
declare namespace util="http://exist-db.org/xquery/util";



declare variable $UsernameToken as xs:string? external;
declare variable $PasswordTokenText as xs:string? external;
declare variable $PasswordTokenDigest as xs:string? external;
declare variable $PasswordTokenCreatedTime as xs:boolean? external;
declare variable $Timestamp_SecondsToLive as xs:int ? external ;
declare variable $NonceRequired as xs:boolean ? external ;
declare variable $TimestampRequired as xs:boolean ? external ;
declare variable $ActionHeader as xs:string? external;

declare function local:get_WS-Policy_1.0($UsernameToken as xs:string?, 
                                           $PasswordTokenText as xs:string?,
                                           $PasswordTokenDigest as xs:string?,  
										   $PasswordTokenCreatedTime as xs:boolean?,  
                                           $Timestamp_SecondsToLive as xs:int ?,
                                           $NonceRequired as xs:boolean ?, 
                                           $TimestampRequired  as xs:boolean ?,
                                           $ActionHeader as xs:string?
										   
                                           ) as element() {

    let $tz:=xs:dayTimeDuration('PT0S')														  
    let $dataTime:=xs:string(adjust-dateTime-to-timezone(current-dateTime(),$tz))
    let $Nonce :=fn-bea:encodeBase64(fn-bea:uuid())

    return

      <Header_WS-Policy_REQ>
				   { if (exists($UsernameToken) or $TimestampRequired)then 
						  <wsse:Security>
							   {if ($TimestampRequired)then 
								   <wsu:Timestamp wsu:Id="{concat('TS-',fn-bea:uuid())}">
											<wsu:Created>{$dataTime}</wsu:Created>
										
										{if (exists($Timestamp_SecondsToLive))then 
											<wsu:Expires>{xs:string(adjust-dateTime-to-timezone(xs:dateTime(fn-bea:addSecondsToDateTime($Timestamp_SecondsToLive,xs:string(current-dateTime()))),$tz))}</wsu:Expires>
										else()
										  }
								   </wsu:Timestamp>
							   else()}
							   
							   {if (exists($UsernameToken))then 
								   <wsse:UsernameToken wsu:Id="{concat('UsernameToken-',fn-bea:uuid())}">
											<wsse:Username>{$UsernameToken}</wsse:Username>
										
										{ if (exists($PasswordTokenText))then 
											<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">{$PasswordTokenText}</wsse:Password>
										else()}
										
										{ if (exists($PasswordTokenDigest))then 
											<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest">{fn-bea:passwordDigest($Nonce,$dataTime,$PasswordTokenDigest)  }</wsse:Password>
										else()}
										
										{if ($NonceRequired)then 
											<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">{$Nonce}</wsse:Nonce>
										else()}
										
										{if ($PasswordTokenCreatedTime)then 
											<wsu:Created>{$dataTime}</wsu:Created>
										else()}
								  </wsse:UsernameToken>
							   else()}
										 
						  </wsse:Security>
				  else()}
				  { if (exists($ActionHeader))then 
						  <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">{$ActionHeader}</wsa:Action>
				  else()}
  
     </Header_WS-Policy_REQ>
	 
};

local:get_WS-Policy_1.0($UsernameToken,$PasswordTokenText,$PasswordTokenDigest,$PasswordTokenCreatedTime,$Timestamp_SecondsToLive,$NonceRequired,$TimestampRequired,$ActionHeader)