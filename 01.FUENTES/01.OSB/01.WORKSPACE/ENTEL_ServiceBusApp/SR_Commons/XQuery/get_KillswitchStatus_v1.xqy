xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/KillswitchStatus/v1";
(:: import schema at "../XSD/ESO/KillswitchStatus_v1_ESO.xsd" ::)

declare variable $ComponentName as xs:string external;
declare variable $ComponentOperation as xs:string? external;
declare variable $ProviderName as xs:string external;
declare variable $ProviderOperation as xs:string? external;
declare variable $KillswitchStatus as xs:string external;
declare variable $KillswitchReason as xs:string external;
declare variable $KillswitchMetadata as element()? external;

declare function local:get_KillswitchStatus_v1($ComponentName as xs:string, 
                                               $ComponentOperation as xs:string?, 
                                               $ProviderName as xs:string, 
                                               $ProviderOperation as xs:string?, 
                                               $KillswitchStatus as xs:string, 
                                               $KillswitchReason as xs:string, 
                                               $KillswitchMetadata as element()?) 
                                               as element() (:: schema-element(ns1:KillswitchStatus) ::) {
    <ns1:KillswitchStatus 
          compName="{fn:data($ComponentName)}" 
          compOperation="{fn:data($ComponentOperation)}" 
          provName="{fn:data($ProviderName)}" 
          provOperation="{fn:data($ProviderOperation)}" 
          ksStatus="{fn:data($KillswitchStatus)}" 
          ksReason="{fn:data($KillswitchReason)}" 
        >
        <ns1:KsMetadata>{$KillswitchMetadata}</ns1:KsMetadata>
    </ns1:KillswitchStatus>
};

local:get_KillswitchStatus_v1($ComponentName, $ComponentOperation, $ProviderName, $ProviderOperation, $KillswitchStatus, $KillswitchReason, $KillswitchMetadata)
