xquery version "1.0" encoding "utf-8";


module namespace ns1="htt://www.entel.cl/sif/MessageHeader/v1";

declare namespace esons_header="http://www.entel.cl/ESO/MessageHeader/v1";

declare namespace esons_jsonheader="http://www.entel.cl/ESO/MessageHeaderJSON/v1";

declare namespace esons_result = "http://www.entel.cl/ESO/Result/v2";

declare namespace esons_error = "http://www.entel.cl/ESO/Error/v1";

declare function ns1:get_RequestHeader($JsonRequestHeader as element()? ) as element()  {
    <esons_header:RequestHeader>
        <esons_header:Consumer sysCode="{fn:data($JsonRequestHeader/esons_jsonheader:Consumer/esons_jsonheader:sysCode)}" enterpriseCode="{fn:data($JsonRequestHeader/esons_jsonheader:Consumer/esons_jsonheader:enterpriseCode)}"
            countryCode="{fn:data($JsonRequestHeader/esons_jsonheader:Consumer/esons_jsonheader:countryCode)}">
        </esons_header:Consumer>
        <esons_header:Trace clientReqTimestamp="{fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:clientReqTimestamp)}" eventID="{fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:eventID)}">
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:reqTimestamp)
                then attribute reqTimestamp {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:reqTimestamp)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:rspTimestamp)
                then attribute rspTimestamp {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:rspTimestamp)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:processID)
                then attribute processID {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:processID)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:sourceID)
                then attribute sourceID {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:sourceID)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:correlationEventID)
                then attribute correlationEventID {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:correlationEventID)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:conversationID)
                then attribute conversationID {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:conversationID)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:correlationID)
                then attribute correlationID {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:correlationID)}
                else ()
            }
            {
                if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service)
                then <esons_header:Service>
                    {
                        if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service/esons_jsonheader:code)
                        then attribute code {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service/esons_jsonheader:code)}
                        else ()
                    }
                    {
                        if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service/esons_jsonheader:name)
                        then attribute name {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service/esons_jsonheader:name)}
                        else ()
                    }
                    {
                        if ($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service/esons_jsonheader:operation)
                        then attribute operation {fn:data($JsonRequestHeader/esons_jsonheader:Trace/esons_jsonheader:Service/esons_jsonheader:operation)}
                        else ()
                    }</esons_header:Service>
                else ()
            }

        </esons_header:Trace>
        {
            if ($JsonRequestHeader/esons_jsonheader:Channel)
            then <esons_header:Channel>
                {
                    if ($JsonRequestHeader/esons_jsonheader:Channel/esons_jsonheader:name)
                    then attribute name {fn:data($JsonRequestHeader/esons_jsonheader:Channel/esons_jsonheader:name)}
                    else ()
                }
                {
                    if ($JsonRequestHeader/esons_jsonheader:Channel/esons_jsonheader:mode)
                    then attribute mode {fn:data($JsonRequestHeader/esons_jsonheader:Channel/esons_jsonheader:mode)}
                    else ()
                }</esons_header:Channel>
            else ()
        }
        {
            if ($JsonRequestHeader/esons_jsonheader:Result)
            then <esons_result:Result status="{fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:status)}">
                {
                    if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:description)
                    then attribute description {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:description)}
                    else ()
                }
                {
                    if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError)
                    then <esons_error:CanonicalError>
                        {
                            if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError/esons_jsonheader:code)
                            then attribute code {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError/esons_jsonheader:code)}
                            else ()
                        }
                        {
                            if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError/esons_jsonheader:description)
                            then attribute description {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError/esons_jsonheader:description)}
                            else ()
                        }
                        {
                            if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError/esons_jsonheader:type)
                            then attribute type {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CanonicalError/esons_jsonheader:type)}
                            else ()
                        }</esons_error:CanonicalError>
                    else ()
                }
                {
                    if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError)
                    then <esons_error:SourceError>
                        {
                            if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:code)
                            then attribute code {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:code)}
                            else ()
                        }
                        {
                            if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:description)
                            then attribute description {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:description)}
                            else ()
                        }
                        <esons_error:ErrorSourceDetails>
                            {
                                if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:source)
                                then attribute source {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:source)}
                                else ()
                            }
                            {
                                if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:details)
                                then attribute details {fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:details)}
                                else ()
                            }
                        </esons_error:ErrorSourceDetails>
                        <esons_error:SourceFault>{fn:data($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:SourceError/esons_jsonheader:SourceFault)}</esons_error:SourceFault></esons_error:SourceError>
                    else ()
                }
                {
                    if ($JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CorrelativeErrors)
                    then <esons_result:CorrelativeErrors>
                        {
                            for $SourceError in $JsonRequestHeader/esons_jsonheader:Result/esons_jsonheader:CorrelativeErrors/esons_jsonheader:SourceError
                            return 
                            <esons_error:SourceError>
                                {
                                    if ($SourceError/esons_jsonheader:code)
                                    then attribute code {fn:data($SourceError/esons_jsonheader:code)}
                                    else ()
                                }
                                {
                                    if ($SourceError/esons_jsonheader:description)
                                    then attribute description {fn:data($SourceError/esons_jsonheader:description)}
                                    else ()
                                }
                                <esons_error:ErrorSourceDetails>
                                    {
                                        if ($SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:source)
                                        then attribute source {fn:data($SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:source)}
                                        else ()
                                    }
                                    {
                                        if ($SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:details)
                                        then attribute details {fn:data($SourceError/esons_jsonheader:ErrorSourceDetails/esons_jsonheader:details)}
                                        else ()
                                    }
                                </esons_error:ErrorSourceDetails>
                                <esons_error:SourceFault>{fn:data($SourceError/esons_jsonheader:SourceFault)}</esons_error:SourceFault></esons_error:SourceError>
                        }</esons_result:CorrelativeErrors>
                    else ()
                }</esons_result:Result>
            else ()
        }

    </esons_header:RequestHeader>
};


declare function ns1:get_ResponseHeader($ResponseHeader as element()?) as element()  {
    <esons_jsonheader:ResponseHeader>
        <esons_jsonheader:Consumer>
            <esons_jsonheader:sysCode>{fn:data($ResponseHeader/*[1]/@sysCode)}</esons_jsonheader:sysCode>
            <esons_jsonheader:enterpriseCode>{fn:data($ResponseHeader/*[1]/@enterpriseCode)}</esons_jsonheader:enterpriseCode>
            <esons_jsonheader:countryCode>{fn:data($ResponseHeader/*[1]/@countryCode)}</esons_jsonheader:countryCode></esons_jsonheader:Consumer>
        <esons_jsonheader:Trace>
            <esons_jsonheader:clientReqTimestamp>{fn:data($ResponseHeader/*[2]/@clientReqTimestamp)}</esons_jsonheader:clientReqTimestamp>
            {
                if ($ResponseHeader/*[2]/@reqTimestamp)
                then <esons_jsonheader:reqTimestamp>{fn:data($ResponseHeader/*[2]/@reqTimestamp)}</esons_jsonheader:reqTimestamp>
                else ()
            }
            {
                if ($ResponseHeader/*[2]/@rspTimestamp)
                then <esons_jsonheader:rspTimestamp>{fn:data($ResponseHeader/*[2]/@rspTimestamp)}</esons_jsonheader:rspTimestamp>
                else ()
            }
            {
                if ($ResponseHeader/*[2]/@processID)
                then <esons_jsonheader:processID>{fn:data($ResponseHeader/*[2]/@processID)}</esons_jsonheader:processID>
                else ()
            }
            <esons_jsonheader:eventID>{fn:data($ResponseHeader/*[2]/@eventID)}</esons_jsonheader:eventID>
            {
                if ($ResponseHeader/*[2]/@sourceID)
                then <esons_jsonheader:sourceID>{fn:data($ResponseHeader/*[2]/@sourceID)}</esons_jsonheader:sourceID>
                else ()
            }
            {
                if ($ResponseHeader/*[2]/@correlationEventID)
                then <esons_jsonheader:correlationEventID>{fn:data($ResponseHeader/*[2]/@correlationEventID)}</esons_jsonheader:correlationEventID>
                else ()
            }
            {
                if ($ResponseHeader/*[2]/@conversationID)
                then <esons_jsonheader:conversationID>{fn:data($ResponseHeader/*[2]/@conversationID)}</esons_jsonheader:conversationID>
                else ()
            }
            {
                if ($ResponseHeader/*[2]/@correlationID)
                then <esons_jsonheader:correlationID>{fn:data($ResponseHeader/*[2]/@correlationID)}</esons_jsonheader:correlationID>
                else ()
            }
            {
                if ($ResponseHeader/*[2]/esons_header:Service)
                then <esons_jsonheader:Service>
                    {
                        if ($ResponseHeader/*[2]/esons_header:Service/@code)
                        then <esons_jsonheader:code>{fn:data($ResponseHeader/*[2]/esons_header:Service/@code)}</esons_jsonheader:code>
                        else ()
                    }
                    {
                        if ($ResponseHeader/*[2]/esons_header:Service/@name)
                        then <esons_jsonheader:name>{fn:data($ResponseHeader/*[2]/esons_header:Service/@name)}</esons_jsonheader:name>
                        else ()
                    }
                    {
                        if ($ResponseHeader/*[2]/esons_header:Service/@operation)
                        then <esons_jsonheader:operation>{fn:data($ResponseHeader/*[2]/esons_header:Service/@operation)}</esons_jsonheader:operation>
                        else ()
                    }</esons_jsonheader:Service>
                else ()
            }
        </esons_jsonheader:Trace>
        {
            if ($ResponseHeader/esons_header:Channel)
            then <esons_jsonheader:Channel>
                {
                    if ($ResponseHeader/esons_header:Channel/@name)
                    then <esons_jsonheader:name>{fn:data($ResponseHeader/esons_header:Channel/@name)}</esons_jsonheader:name>
                    else ()
                }
                {
                    if ($ResponseHeader/esons_header:Channel/@mode)
                    then <esons_jsonheader:mode>{fn:data($ResponseHeader/esons_header:Channel/@mode)}</esons_jsonheader:mode>
                    else ()
                }</esons_jsonheader:Channel>
            else ()
        }
        <esons_jsonheader:Result>
            <esons_jsonheader:status>{fn:data($ResponseHeader/esons_result:Result/@status)}</esons_jsonheader:status>
            {
                if ($ResponseHeader/esons_result:Result/@description)
                then <esons_jsonheader:description>{fn:data($ResponseHeader/esons_result:Result/@description)}</esons_jsonheader:description>
                else ()
            }
            {
                if ($ResponseHeader/esons_result:Result/esons_error:CanonicalError)
                then <esons_jsonheader:CanonicalError>
                    {
                        if ($ResponseHeader/esons_result:Result/esons_error:CanonicalError/@type)
                        then <esons_jsonheader:type>{fn:data($ResponseHeader/esons_result:Result/esons_error:CanonicalError/@type)}</esons_jsonheader:type>
                        else ()
                    }
                    {
                        if ($ResponseHeader/esons_result:Result/esons_error:CanonicalError/@code)
                        then <esons_jsonheader:code>{fn:data($ResponseHeader/esons_result:Result/esons_error:CanonicalError/@code)}</esons_jsonheader:code>
                        else ()
                    }
                    {
                        if ($ResponseHeader/esons_result:Result/esons_error:CanonicalError/@description)
                        then <esons_jsonheader:description>{fn:data($ResponseHeader/esons_result:Result/esons_error:CanonicalError/@description)}</esons_jsonheader:description>
                        else ()
                    }</esons_jsonheader:CanonicalError>
                else ()
            }
            {
                if ($ResponseHeader/esons_result:Result/esons_error:SourceError)
                then <esons_jsonheader:SourceError>
                    {
                        if ($ResponseHeader/esons_result:Result/esons_error:SourceError/@code)
                        then <esons_jsonheader:code>{fn:data($ResponseHeader/esons_result:Result/esons_error:SourceError/@code)}</esons_jsonheader:code>
                        else ()
                    }
                    {
                        if ($ResponseHeader/esons_result:Result/esons_error:SourceError/@description)
                        then <esons_jsonheader:description>{fn:data($ResponseHeader/esons_result:Result/esons_error:SourceError/@description)}</esons_jsonheader:description>
                        else ()
                    }
                    <esons_jsonheader:ErrorSourceDetails>
                        {
                            if ($ResponseHeader/esons_result:Result/esons_error:SourceError/esons_error:ErrorSourceDetails/@source)
                            then <esons_jsonheader:source>{fn:data($ResponseHeader/esons_result:Result/esons_error:SourceError/esons_error:ErrorSourceDetails/@source)}</esons_jsonheader:source>
                            else ()
                        }
                        {
                            if ($ResponseHeader/esons_result:Result/esons_error:SourceError/esons_error:ErrorSourceDetails/@details)
                            then <esons_jsonheader:details>{fn:data($ResponseHeader/esons_result:Result/esons_error:SourceError/esons_error:ErrorSourceDetails/@details)}</esons_jsonheader:details>
                            else ()
                        }
                    </esons_jsonheader:ErrorSourceDetails>
                    <esons_jsonheader:SourceFault>{fn-bea:serialize($ResponseHeader/esons_result:Result/esons_error:SourceError/esons_error:SourceFault)}</esons_jsonheader:SourceFault></esons_jsonheader:SourceError>
                else ()
            }
            {
                if ($ResponseHeader/esons_result:Result/esons_result:CorrelativeErrors)
                then <esons_jsonheader:CorrelativeErrors>
                    {
                        for $SourceError in $ResponseHeader/esons_result:Result/esons_result:CorrelativeErrors/esons_error:SourceError
                        return 
                        <esons_jsonheader:SourceError>
                            {
                                if ($SourceError/@code)
                                then <esons_jsonheader:code>{fn:data($SourceError/@code)}</esons_jsonheader:code>
                                else ()
                            }
                            {
                                if ($SourceError/@description)
                                then <esons_jsonheader:description>{fn:data($SourceError/@description)}</esons_jsonheader:description>
                                else ()
                            }
                            <esons_jsonheader:ErrorSourceDetails>
                                {
                                    if ($SourceError/esons_error:ErrorSourceDetails/@source)
                                    then <esons_jsonheader:source>{fn:data($SourceError/esons_error:ErrorSourceDetails/@source)}</esons_jsonheader:source>
                                    else ()
                                }
                                {
                                    if ($SourceError/esons_error:ErrorSourceDetails/@details)
                                    then <esons_jsonheader:details>{fn:data($SourceError/esons_error:ErrorSourceDetails/@details)}</esons_jsonheader:details>
                                    else ()
                                }
                            </esons_jsonheader:ErrorSourceDetails>
                            <esons_jsonheader:SourceFault>{fn-bea:serialize($SourceError/esons_error:SourceFault)}</esons_jsonheader:SourceFault></esons_jsonheader:SourceError>
                    }</esons_jsonheader:CorrelativeErrors>
                else ()
            }
        </esons_jsonheader:Result>
    </esons_jsonheader:ResponseHeader>
};