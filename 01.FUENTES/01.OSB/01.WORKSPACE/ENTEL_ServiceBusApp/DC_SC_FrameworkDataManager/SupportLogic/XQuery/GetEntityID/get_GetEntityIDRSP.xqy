xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://www.entel.cl/SC/FrameworkDataManager/getEntityID/v1";
(:: import schema at "../../../SupportAPI/XSD/CSM/getEntityID_FrameworkDataManager_v1_CSM.xsd" ::)

declare namespace ns4 = "http://www.entel.cl/ESO/Error/v1";
declare namespace ns3 = "http://www.entel.cl/ESO/Result/v2";

declare namespace ns1 = "http://www.entel.cl/SC/FrameworkDataManager/FRWEntities/Aux";
(:: import schema at "../../../SupportAPI/XSD/FRWEntities.xsd" ::)

declare variable $JcaRSP as element() external;
declare variable $Result as element() external;

declare function local:func($JcaRSP as element(), $Result as element() ) as element() (:: schema-element(ns2:GetEntityIDRSP) ::) {
    <ns2:GetEntityIDRSP>
         {
          if(
            (data($JcaRSP/*:P_SOURCE_ERROR_CODE)='0')) then
              <ns1:EntityID>{data($JcaRSP/*[1])}</ns1:EntityID>
          else
            <ns3:Result>{$Result/*}</ns3:Result>
         }
    </ns2:GetEntityIDRSP>
};

local:func($JcaRSP,$Result)
