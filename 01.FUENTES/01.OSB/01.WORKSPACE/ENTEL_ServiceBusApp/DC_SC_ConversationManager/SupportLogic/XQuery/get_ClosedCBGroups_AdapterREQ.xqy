xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/getClosedCBGroups";
(:: import schema at "../JCA/getClosedCBGroups/getClosedCBGroups_sp.xsd" ::)
declare namespace ns1="http://xmlns.oracle.com/pcbpel/adapter/db/top/getClosedGroups_Poller";
(:: import schema at "../JCA/getClosedGroups_Poller/getClosedGroups_Poller_table.xsd" ::)

declare variable $CBClosedGroups as element() (:: schema-element(ns1:EsbConversationCbGroupCollection) ::) external;

declare function local:get_ClosedCBGroups_AdapterREQ($CBClosedGroups as element() (:: schema-element(ns1:EsbConversationCbGroupCollection) ::)) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_CB_GRP_TAGS>
            
            {
            for $EsbConversationCbGroup in $CBClosedGroups/ns1:EsbConversationCbGroup
            return <ns2:P_CB_GRP_TAGS_ITEM>{fn:data($EsbConversationCbGroup/ns1:cbGrpTag)}</ns2:P_CB_GRP_TAGS_ITEM>
            }

        </ns2:P_CB_GRP_TAGS>
    </ns2:InputParameters>
};

local:get_ClosedCBGroups_AdapterREQ($CBClosedGroups)
