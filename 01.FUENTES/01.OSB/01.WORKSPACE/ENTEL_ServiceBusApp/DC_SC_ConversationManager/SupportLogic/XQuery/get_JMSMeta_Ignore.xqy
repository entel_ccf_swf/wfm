xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)


declare function local:get_JMSMeta_Ignore() as element() {
    <JMSMeta
      jmsResponse="{'IGNORE'}"
    />
};

local:get_JMSMeta_Ignore()
