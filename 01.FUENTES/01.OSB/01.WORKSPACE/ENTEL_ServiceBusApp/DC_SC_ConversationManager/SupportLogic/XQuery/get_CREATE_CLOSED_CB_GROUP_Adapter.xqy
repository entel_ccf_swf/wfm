xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/SC/ConversationManager/closeAggrGroup/v1";
(:: import schema at "../../SupportAPI/XSD/CSM/closeAggrGroup_ConversationManager_v1_CSM.xsd" ::)
declare namespace ns2="http://xmlns.oracle.com/pcbpel/adapter/db/sp/createClosedCBGroup";
(:: import schema at "../JCA/createClosedCBGroup/createClosedCBGroup_sp.xsd" ::)

declare namespace cal = "http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription";

declare variable $CbGroupTag as xs:string external;
declare variable $JMSMeta as element() external;

declare function local:get_CREATE_CLOSED_CB_GROUP_Adapter($CbGroupTag as xs:string, $JMSMeta as element()) as element() (:: schema-element(ns2:InputParameters) ::) {
    <ns2:InputParameters>
        <ns2:P_CB_GRP_TAG>{$CbGroupTag}</ns2:P_CB_GRP_TAG>
        <ns2:P_CB_GRP_OWN_JMS_TYPE>{fn:data($JMSMeta/@jmsMessageType)}</ns2:P_CB_GRP_OWN_JMS_TYPE>
        <ns2:P_CB_GRP_OWN_JMS_MSGID>{fn:data($JMSMeta/@jmsCorrelationID)}</ns2:P_CB_GRP_OWN_JMS_MSGID>
    </ns2:InputParameters>
};

local:get_CREATE_CLOSED_CB_GROUP_Adapter($CbGroupTag,$JMSMeta)
