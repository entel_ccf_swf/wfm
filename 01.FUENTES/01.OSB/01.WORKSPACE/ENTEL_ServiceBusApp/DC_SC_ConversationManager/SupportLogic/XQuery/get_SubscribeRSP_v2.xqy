xquery version "1.0" encoding "utf-8";

(:: OracleAnnotationVersion "1.0" ::)

declare namespace ns1="http://www.entel.cl/ESO/Result/v2";
(:: import schema at "../../../SR_Commons/XSD/ESO/Result_v2_ESO.xsd" ::)
declare namespace ns2="http://www.entel.cl/SC/ConversationManager/subscribe/v2";
(:: import schema at "../../SupportAPI/XSD/CSM/subscribe_ConversationManager_v2_CSM.xsd" ::)

declare namespace ns3 = "http://www.entel.cl/ESO/Error/v1";

declare variable $Result as element() (:: schema-element(ns1:Result) ::) external;
declare variable $CallbackMessage as element() external;

declare function local:get_SubscribeRSP_v2($Result as element() (:: schema-element(ns1:Result) ::), $CallbackMessage as element()) as element() (:: schema-element(ns2:SubscribeRSP) ::) {
    <ns2:SubscribeRSP>
       {
            $Result
       }
      <ns2:CallbackMessage>{$CallbackMessage}</ns2:CallbackMessage>
    </ns2:SubscribeRSP>
};

local:get_SubscribeRSP_v2($Result,$CallbackMessage)
