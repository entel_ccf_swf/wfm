<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:socket="http://www.oracle.com/XSL/Transform/java/oracle.tip.adapter.socket.ProtocolTranslator" xmlns:oracle-xsl-mapper="http://www.oracle.com/xsl/mapper/schemas" xmlns:dvm="http://www.oracle.com/XSL/Transform/java/oracle.tip.dvm.LookupValue" xmlns:mhdr="http://www.oracle.com/XSL/Transform/java/oracle.tip.mediator.service.common.functions.MediatorExtnFunction" xmlns:oraxsl="http://www.oracle.com/XSL/Transform/java" xmlns:oraext="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.ExtFunc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xp20="http://www.oracle.com/XSL/Transform/java/oracle.tip.pc.services.functions.Xpath20" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ns0="http://www.entel.cl/SC/RoutingManager/route/v1" xmlns:xref="http://www.oracle.com/XSL/Transform/java/oracle.tip.xref.xpath.XRefXPathFunctions" exclude-result-prefixes=" oracle-xsl-mapper xsi xsd xsl ns0 socket dvm mhdr oraxsl oraext xp20 xref"
                xmlns:ns1="http://www.entel.cl/ESO/MessageHeader/v1"
                xmlns:ns2="http://www.entel.cl/SC/CorrelationManager/Aux/RoutingMetadata"
                xmlns:ns3="http://www.entel.cl/SC/ConversationManager/Aux/CallbackSubscription"
                xmlns:wsoap12="http://schemas.xmlsoap.org/wsdl/soap12/"
                xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:tns="http://www.entel.cl/SC/RoutingManager/v1"
                xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:ns5="http://www.entel.cl/EDD/Dictionary/v1"
                xmlns:ns6="http://www.entel.cl/ESO/Error/v1" xmlns:ns4="http://www.entel.cl/ESO/Result/v2">
   <oracle-xsl-mapper:schema>
      <!--SPECIFICATION OF MAP SOURCES AND TARGETS, DO NOT MODIFY.-->
      <oracle-xsl-mapper:mapSources>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_RoutingManager/SupportAPI/WSDL/RoutingManager.wsdl"/>
            <oracle-xsl-mapper:rootElement name="RouteRSP" namespace="http://www.entel.cl/SC/RoutingManager/route/v1"/>
         </oracle-xsl-mapper:source>
         <oracle-xsl-mapper:source type="WSDL">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_RoutingManager/SupportAPI/WSDL/RoutingManager.wsdl"/>
            <oracle-xsl-mapper:rootElement name="RouteREQ" namespace="http://www.entel.cl/SC/RoutingManager/route/v1"/>
            <oracle-xsl-mapper:param name="input_RouteREQ.body"/>
         </oracle-xsl-mapper:source>
      </oracle-xsl-mapper:mapSources>
      <oracle-xsl-mapper:mapTargets>
         <oracle-xsl-mapper:target type="WSDL">
            <oracle-xsl-mapper:schema location="oramds:/apps/Commons/DC_SC_RoutingManager/SupportAPI/WSDL/RoutingManager.wsdl"/>
            <oracle-xsl-mapper:rootElement name="RouteRSP" namespace="http://www.entel.cl/SC/RoutingManager/route/v1"/>
         </oracle-xsl-mapper:target>
      </oracle-xsl-mapper:mapTargets>
      <!--GENERATED BY ORACLE XSL MAPPER 12.2.1.0.0(XSLT Build 151013.0700.0085) AT [WED FEB 22 17:42:52 ART 2017].-->
   </oracle-xsl-mapper:schema>
   <!--User Editing allowed BELOW this line - DO NOT DELETE THIS LINE-->
   <xsl:param name="input_RouteREQ.body"/>
   <xsl:template match="/">
      <ns0:RouteRSP>
          <xsl:copy-of select="/ns0:RouteRSP/ns4:Result"/>
          <xsl:copy-of select="/ns0:RouteRSP/ns1:RequestHeader"/>
          <xsl:copy-of select="$input_RouteREQ.body/ns0:RouteREQ/ns2:RouteMeta"/>
          <xsl:copy-of select="/ns0:RouteRSP/ns2:RouteTo"/>
          <xsl:copy-of select="/ns0:RouteRSP/ns0:REQMsg"/>
          <xsl:copy-of select="/ns0:RouteRSP/ns0:RSPMsg"/>
      </ns0:RouteRSP>
   </xsl:template>
</xsl:stylesheet>
